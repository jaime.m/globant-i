let test1 = [2, 4,1,10, 11, -1, -4, null, undefined, "bad", NaN];

const ordenar =(vc)=>{
   return  vc.filter(x=> (typeof x ==='number') && (!!x)).sort((a,b)=>
       a-b)
}
console.log(
ordenar(test1));



//autodesk exercise

/*
=================
Base Exercise:
=================
A string is said to be beautiful if "b" occurs in it no more times than "a"; "c" occurs in it no more times than "b"; etc.
Given a string s, check whether it is beautiful.
Example
For s = "bbbaacdafe", the output should be true;
a   b   c   d   e   f   g ...  z
|   |   |   |   |   |   |      |
3 ≥ 3 > 1 ≥ 1 ≥ 1 ≥ 1 > 0 ... ≥0
It's so beautiful ;-)```
For `s = "aabbb"`, the output should be `false`
[execution time limit] 4 seconds (js)
*/
console.time("test_example");
const isBeautifulString = (inputString) => {
const vector = inputString.toLowerCase().split('');

const data = vector.reduce((acc,prev)=>{
    acc[prev]?acc[prev]++:acc[prev]=1;
    return acc;
},{});
  console.log("This prints to the console when you Run Tests");

const sortable = Object.fromEntries(
    Object.entries(data).sort(([a,c],[b,d]) =>{
        return b-a;
    } )
);
let previous=-1;
let status=true;
for (var letter in sortable) {
    if(previous==-1){
        previous=sortable[letter];
        continue;
    }
    if(previous<sortable[letter]){
        status=false;
        break;
    }
    previous=sortable[letter];

}
    return "Hello, ",status;
};

console.log(
  isBeautifulString('s = "aabbb"') 
);

console.timeEnd("test_example");

